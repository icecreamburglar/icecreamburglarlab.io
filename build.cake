#tool "nuget:?package=Wyam"
#addin "Cake.Powershell"

var RootDir = MakeAbsolute(Directory(".")); 
var buildtarget = Argument("target", "Default");
var waymexe = "wyam"; //RootDir + "/tools/wyam/Wyam/tools/net462/Wyam.exe";

Task("Build")
    .Does(() => StartProcess(waymexe, "build --output output/public"));

Task("Preview")
    .Does(() => StartProcess(waymexe, "-p -w"));

Task("Deploy")
    .IsDependentOn("Build")
    .Does(() => 
    {
        CopyFile("./gitlab-ci.yml", "output/gitlab-ci.yml");
        if(FileExists("./CNAME"))
            CopyFile("./CNAME", "output/CNAME");

        StartProcess("git", "checkout master");
        StartProcess("git", "add .");
        StartProcess("git", "commit -m \"Checking output in for subtree\"");
        StartProcess("git", "subtree split --prefix output -b pages");
        StartProcess("git", "push -f origin pages:pages");
        StartProcess("git", "branch -D pages");

    });

RunTarget(buildtarget);