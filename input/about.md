Title: About Me
---
I'm a hobbyist programmer, mostly spending time in c#.
Though currently I am trying to broaden my fluidity in other languages, such as:
  * F#
  * Haskell
  * Python
  * Rust
  * Assembly (arm)

  I've been programming for the last 10 years now.
  I started in vb.net and Visual Studio 2008.
  A few years later I moved to C# and never looked back to VB.
  
  I primarily use one-of-two OSes on a dual-booted system, running Windows 10 and Linux Manjaro.
  I also, however, have a Dell Inspiron 7000, and various Toshiba Satellite laptops from the 2008-2011 time-frame.
  
  Additionally, I enjoy tinkering with Arduinos and RaspberryPis, though I've not finished any significant project on either platform.